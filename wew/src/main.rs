fn main() {
    let x = 5;
    let y = 10;
    let z = x + y;
    // println!("Hello, world!");
    match z {
        5 => println!("{} is correct.", z),
        10 => println!("{} is correct.", z),
        15 => println!("{} is correct.", z),
        _ => {}
    };
}

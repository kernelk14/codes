var x = 5
print(x += 1)
var i = 0

struct Names {
    var name = ''
    var age = 0
}

for i in 1...5 {
    var i = 0
    print(i += 1)
}
var nm = Names()

var nm.name = "Khyle"
var nm.age = 17

print("My name is \(nm.name) and I'm \(nm.age) y/o.")

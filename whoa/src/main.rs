use std::vec::Vec;

fn analyze(program: &str) {
    let consumed: Vec<_> = program.chars().collect();
    //let dict: Vec<_> = Vec::new();
    let mut ip: usize = 0;
    let mut c = vec![];
    for consumed in program.split(',') {
        println!("char {}: {}", ip, consumed);
        println!("Vector of string slices: {:#?}", c);
        match consumed.nth(ip) {
            " " => println!("Oops, there is a whitespace."),
            "name" => {
                println!("I saw a name.");
                c.push(consumed);
            },
            _ => {}
        }
        ip += 1;
    }
}

fn main() {
    let program: &str = "name, 'khyle'";
    return analyze(&program);
}

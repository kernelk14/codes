package main

import "fmt"

func main() {
    a := 10
    b := 20
    c := a + b

    if c == 10 {
        fmt.Printf("%d is correct.\n", c)
    } else if c == 20 {
        fmt.Printf("%d is correct.\n", c)
    } else if c == 30 {
        fmt.Printf("%d is correct.\n", c)
    } else {
        fmt.Printf("Undefined")
    }
    // fmt.Println("Hello World!")
}

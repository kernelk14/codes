#include <iostream>
#include <assert.h>
using namespace std;

static int a;
static int b;
static int c;

int warn(c) {
  printf("%d ,This is correct.\n");
}

int main(int argc, char **argv) {
  a = 10;
  b = 20;
  c = a + b;
  
  if (c == 10) {
    warn(c);
  } else if (c == 20) {
    warn(c);
  } else if (c == 30) {
    warn(c);
  } else {
    fprintf(stderr, "Uncorrect.\n");
  }
}
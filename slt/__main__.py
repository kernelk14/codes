#!/usr/bin/env python3
import os
import sys
import re

def split(word):
    return [char for char in word]

def prappend(letter):
    x = {
        'Letter': letter
    }
    print(x)

program = split("\"hello world!\"")
ip = 0
stack = []

# TODO: Find way to push characters

a = 0x65
b = 0x66
c = 0x67
d = 0x68
e = 0x69
f = 0x70
g = 0x71
h = 0x72
i = 0x73
j = 0x74
k = 0x75
l = 0x76
m = 0x77
n = 0x78
o = 0x79
p = 0x80
q = 0x81
r = 0x82
s = 0x83
t = 0x84
u = 0x85
v = 0x86
w = 0x87
x = 0x88
y = 0x89
z = 0x90
SPC = 0x40
EM = 0x41

for ip in range(len(program)):
    letter = program[ip]
    if letter.startswith('"'):
        ip += 1
        continue
    elif letter == 'a':
        prappend(a)
    elif letter == 'b':
        prappend(b)
    elif letter == 'c':
        prappend(c)
    elif letter == 'd':
        prappend(d)
    elif letter == 'e':
        prappend(e)
    elif letter == 'f':
        prappend(f)
    elif letter == 'g':
        prappend(g)
    elif letter == 'h':
        prappend(h)
    elif letter == 'i':
        prappend(i)
    elif letter == 'j':
        prappend(j)
    elif letter == 'k':
        prappend(k)
    elif letter == 'l':
        prappend(l)
    elif letter == 'm':
        prappend(m)
    elif letter == 'n':
        prappend(n)
    elif letter == 'o':
        prappend(o)
    elif letter == 'p':
        prappend(p)
    elif letter == 'q':
        prappend(q)
    elif letter == 'r':
        prappend(r)
    elif letter == 's':
        prappend(s)
    elif letter == 't':
        prappend(t)
    elif letter == 'u':
        prappend(u)
    elif letter == 'v':
        prappend(v)
    elif letter == 'w':
        prappend(w)
    elif letter == 'x':
        prappend(x)
    elif letter == 'y':
        prappend(y)
    elif letter == 'z':
        prappend(z)
    elif letter == ' ':
        prappend(SPC)
    elif letter == '!':
        prappend(EM)
    else:
        print("Unknown object")
    # print(letter)
    # print(f"Stack: {stack}")
    ip += 1


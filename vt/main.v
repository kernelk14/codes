fn main() {
    a := 5
    b := 10
    c := a + b
    if c == 10 {
        println('$c is correct.')
    } else if c == 15 {
        println('$c is correct.')
    } else if c == 20 {
        println('$c is correct.')
    } else {
        assert false
    }
}

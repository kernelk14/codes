#include <unistd.h>

void main() {
    const char msg[] = "Hello, World!\n";
    write(0, msg, sizeof(msg));
    exit(0);
}

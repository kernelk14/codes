	.text
	.file	"hesrc.c"
	.globl	main                            // -- Begin function main
	.p2align	2
	.type	main,@function
main:                                   // @main
	.cfi_startproc
// %bb.0:
	sub	sp, sp, #48
	stp	x29, x30, [sp, #32]             // 16-byte Folded Spill
	add	x29, sp, #32
	.cfi_def_cfa w29, 16
	.cfi_offset w30, -8
	.cfi_offset w29, -16
	adrp	x8, .L__const.main.msg
	add	x8, x8, :lo12:.L__const.main.msg
	ldr	x9, [x8]
	add	x1, sp, #16
	str	x9, [sp, #16]
	ldur	x8, [x8, #7]
	stur	x8, [x1, #7]
	mov	w0, wzr
	str	w0, [sp, #12]                   // 4-byte Folded Spill
	mov	x2, #15
	bl	write
	ldr	w0, [sp, #12]                   // 4-byte Folded Reload
	bl	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
                                        // -- End function
	.type	.L__const.main.msg,@object      // @__const.main.msg
	.section	.rodata.str1.1,"aMS",@progbits,1
.L__const.main.msg:
	.asciz	"Hello, World!\n"
	.size	.L__const.main.msg, 15

	.ident	"clang version 14.0.6"
	.section	".note.GNU-stack","",@progbits

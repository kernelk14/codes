#!/usr/bin/env python3
from libptsd import *

a = 10
b = 20
c = a + b

if c == 10:
    printf("%d is Correct.\n" % c)
elif c == 20:
    printf("%d is Correct.\n" % c)
elif c == 30:
    printf("%d is Correct.\n" % c)
else:
    eprintf("ERROR: It's undefined.\n")
    exit(1)

use std::io::{self, Write};
pub fn write(string: &str) -> io::Result<()> {
    io::stdout().write_all(&string)?;
    Ok(())
}

fn main() {
    println!("Hello World");
}
